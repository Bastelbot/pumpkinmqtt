#include <Adafruit_NeoPixel.h>

#define LED_PIN        D3     // LED Pin
#define NUM_LED        10 //2 // Number of LEDs

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_LED, LED_PIN, NEO_GRB + NEO_KHZ800);

unsigned char display_grid[NUM_LED];
unsigned char led_gn = 0;
unsigned char led_bl = 0;
unsigned char led_rd = 0;

void setupDisplay()
{
  pixels.begin(); // This initializes the NeoPixel library.
  for(unsigned char i=0; i<NUM_LED; i++) {
    display_grid[i] = 0;
    // Color(red, green, blue)
    pixels.setPixelColor(i, pixels.Color(0,0,0)); // switched off.
  }
}

void clearDisplay()
{
  for(unsigned char i=0; i<NUM_LED; i++) {
    display_grid[i] = 0;
  }
}

void setPosition(unsigned char pos, unsigned char intensity)
{
  display_grid[pos] = intensity;
}

void setColor(unsigned char red, unsigned char green, unsigned char blue)
{
  led_gn = green;
  led_bl = blue;
  led_rd = red;
}

void updateDisplay()   // update display with predefined values
{
  unsigned char gn, bl, rd;
  for(unsigned char i=0; i<NUM_LED; i++) {
    gn = (display_grid[i] * led_gn) >> 8;
    bl = (display_grid[i] * led_bl) >> 8;
    rd = (display_grid[i] * led_rd) >> 8;
    pixels.setPixelColor(i, pixels.Color(rd,gn,bl));
  }
  pixels.show(); // This sends the updated pixel color to the hardware.  
}

