unsigned int action_counter = 0;

void do_action_off() {
  clearDisplay();
  updateDisplay();
}

void do_action_normal() {
  unsigned char gn = random(20, 25);
  setColor(35, gn, 0);
  setPosition(0, random(150, 250));
  setPosition(1, random(150, 250));
  updateDisplay();
}

void do_action_crazy() {
  setColor(0, 250, 0);
  //unsigned char value = random(0, 6) * 50;
  unsigned char value = random(0, 4) * 85;
  setPosition(0, value);
  setPosition(1, value);
  updateDisplay();
}

unsigned int action_flash_sleep = 0;

void do_action_flash() {
  if(action_counter > action_flash_sleep) {
    action_counter = 0;
    action_flash_sleep = random(30, 250);
  }
  setColor(50, 50, 250);
  unsigned char value = 0;
  switch(action_counter) {
    case 0:
    case 1:
    case 2:
    case 10:
    case 12:
      value = 255;
      break;
  }
  setPosition(0, value);
  setPosition(1, value);
  updateDisplay();
}

void do_action_default() {
  return do_action_normal();
}

void do_action(char action) {
  action_counter++;
  switch(action) {
    case '\0': return do_action_default();
    case 'o': return do_action_off();
    case 'n': return do_action_normal();
    case 'c': return do_action_crazy();
    case 'f': return do_action_flash();
    default: return;
  }
}

