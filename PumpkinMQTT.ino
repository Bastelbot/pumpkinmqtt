#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include "config.h"

// Code adapted from https://smarthome-blogger.de/tutorial/esp8266-mqtt-tutorial/

WiFiClient espClient;
PubSubClient client(espClient);

char current_action = '\0';
unsigned int default_action_counter = 0;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  setupDisplay();
  delay(1000);

  Serial.begin(9600);
  setup_wifi();
  client.setServer(MQTT_BROKER, MQTT_PORT);
  client.setCallback(callback);

  digitalWrite(LED_BUILTIN, HIGH);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  WiFi.begin(WIFI_SSID, WIFI_PASSWD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (!client.connect("ESP8266Client", MQTT_USER, MQTT_PASSWD)) {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
  client.subscribe(MQTT_TOPIC);
  Serial.println("MQTT connected...");
}

void callback(char* topic, byte* payload, unsigned int length) {
  digitalWrite(LED_BUILTIN, LOW);
  Serial.print("Received message [");
  Serial.print(topic);
  Serial.print("] ");
  char msg[length+1];
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    msg[i] = (char)payload[i];
  }
  Serial.println();
 
  msg[length] = '\0';
  Serial.println(msg);

  if(strcmp(msg,"off")==0)     current_action = 'o';
  if(strcmp(msg,"normal")==0)  current_action = 'n';
  if(strcmp(msg,"crazy")==0)   current_action = 'c';
  if(strcmp(msg,"flash")==0)   current_action = 'f';
  if(strcmp(msg,"default")==0) current_action = '\0';
  default_action_counter = 0;

  digitalWrite(LED_BUILTIN, HIGH);
}


void loop() {
  // Do work:
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  do_action(current_action);
  default_action_counter++;
  if(default_action_counter > DEFAULT_ACTION_TIMEOUT) {
    default_action_counter = 0;
    if(current_action != '\0') client.publish(MQTT_TOPIC, "default");
  }
  delay(100);
}


