# PumpkinMQTT

MQTT controlled Halloween pumpkin.
An ESP8266 receives commands from a MQTT broker and illuminates the pumpkin using a few NeoPixels.